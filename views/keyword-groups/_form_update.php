<?php

use app\modules\admin\models\Project;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\KeywordGroups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keyword-groups-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default panel-body">

        <div class="row">

            <div class="col-md-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>


        </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
