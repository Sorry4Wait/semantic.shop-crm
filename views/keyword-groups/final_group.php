<?php

use yii\db\Query;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keyword-groups-index">

    <p>

    </p>
    <div class="panel panel-default panel-body">



        <?php Pjax::begin(['id' => 'pjax_grid']); ?>

        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function($model) {
                        return ['value' => $model->id];
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'attribute' => 'name',
                    'content' => function($model){
                        return  $model->name;
                    },
                    'contentOptions' => ['style' => 'width: 20%;white-space: nowrap;'],
                    'filter' => false,
                ],
                [
                    'attribute' => 'semantik',
                    'value' => function ($model){
                        $user = \app\modules\admin\models\User::findOne(['id' => $model->semantik]);
                        return $user->username;
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'frequency',
                    'filter' => false,
                ],
                [
                    'attribute' => 'keys',
                    'value' => function ($model){

                        return $model->getCountKeywords();

                    },

                    'format' => 'raw',
                    'filter' => true,

                ],
                [
                    'attribute' => 'import_key',
                    'value' => function($searchModel){
                        $a = $searchModel->getImportKey();
                        return $a[$searchModel->import_key];
                    },
                    'filterInputOptions' => [
                        'class' => 'form-control',
                        'prompt' => 'Все',
                    ],
                    'filter'=> array(
                        '1' => 'НЧ',
                        '2' => 'ВЧ',
                        '3' => 'На потом',
                    ),

                ],
                [
                    'attribute' => 'project',
                    'value' => function($searchModel){
                        $project = \app\modules\admin\models\Project::findOne(['id' => $searchModel->project]);
                        return $project->name;
                    },
                    'filter' => false,

                ],
                [
                    'attribute' => 'status',
                    'value' => function($searchModel){
                        $a = \app\modules\admin\models\KeywordGroups::getStatusesLabel();
                        return $a[$searchModel->status];
                    },
                    'filter' => false,

                ],
                [
                    'attribute' => 'in_work',
                    'filter' => false,
                ],



                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>


<script type="text/javascript">


    function deleteSelected() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить выбранные Группы?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_late() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('На потом?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-late',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function select() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Принять готовые группы?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'check',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

</script>


