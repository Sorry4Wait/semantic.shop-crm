<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Главное меню', 'options' => ['class' => 'header']],
                    ['label' => 'ТЗ',
                        'items' => [
                            [
                                'label' => 'Исходные ключевики',
                                'icon' => 'calendar-o',
                                'url' => ['site/status1', 'sort' => 'fire'],
                                'active' => Yii::$app->request->get('status') == 1 ||
                                    $this->context->route == 'site/status1',
                            ],
                            [
                                'label' => 'В работе',
                                'icon' => 'calendar-plus-o',
                                'url' => ['site/projects-status', 'status' => '4'],
                                'active' => Yii::$app->request->get('status') == 4 ||
                                    $this->context->route == 'site/status4',

                            ],
                            [
                                'label' => 'Готовое ТЗ',
                                'icon' => 'calendar-check-o',
                                'url' => ['site/projects-status', 'status' => '2'],
                                'active' => Yii::$app->request->get('status') == 2 ||
                                    $this->context->route == 'site/status2'
                                ,
                            ],
                        ],
                    ],

                    [
                        'label' => 'Семантика',
                        'icon' => 'align-justify',

                        'items' => [
                            [
                                'label' => 'Исходные группы',

                                'url' => ['keyword-groups/index'],
                            ],
                            [
                                'label' => 'В работе',

                                'url' => ['keyword-groups/show', 'status' => '2'],
                            ],
                            [
                                'label' => 'На модерации',

                                'url' => ['keyword-groups/show', 'status' => '3'],
                            ],
                            [
                                'label' => 'Обработка групп',

                                'url' => ['keyword-groups/show', 'status' => '4'],
                            ],
                            [
                                'label' => 'Проверка групп',

                                'url' => ['keyword-groups/show', 'status' => '5'],
                            ],
                            [
                                'label' => 'Готовые группы',

                                'url' => ['keyword-groups/final-group'],
                            ],
                            [
                                'label' => 'Использованные',

                                'url' => ['keyword-groups/used-group'],
                            ],
                        ],
                    ],

                ],
            ]
        ) ?>

    </section>

</aside>
