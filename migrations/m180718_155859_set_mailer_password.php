<?php

use yii\db\Migration;

/**
 * Class m180718_155859_set_mailer_password
 */
class m180718_155859_set_mailer_password extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableSchema = Yii::$app->db->schema->getTableSchema('options');

        if ($tableSchema != null) {
            $key = \app\modules\admin\models\Options::find(['key' => 'site_email_password'])->one();


            if (!$key) {
                $this->insert('options', [
                    'key' => 'site_email_password',
                    'value' => 'testsemantic123',
                ]);
            }
        }
    }
}
