<?php

use yii\db\Migration;

/**
 * Class m181016_074205_add_new_column_to_table_project
 */
class m181016_074205_add_new_column_to_table_project extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('project', 'tz_binet', $this->integer()->defaultValue(0));
    }


    public function down()
    {
        echo "m181016_074205_add_new_column_to_table_project cannot be reverted.\n";

        return false;
    }

}
