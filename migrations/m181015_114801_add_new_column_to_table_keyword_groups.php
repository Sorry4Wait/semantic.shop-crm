<?php

use yii\db\Migration;

/**
 * Class m181015_114801_add_new_column_to_table_keyword_groups
 */
class m181015_114801_add_new_column_to_table_keyword_groups extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('keyword_groups', 'accept_time', $this->dateTime());
    }

    public function down()
    {
        echo "m181015_114801_add_new_column_to_table_keyword_groups cannot be reverted.\n";

        return false;
    }

}
