<?php

use yii\db\Migration;

/**
 * Class m180715_163515_set_mailer_params
 */
class m180715_163515_set_mailer_params extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableSchema = Yii::$app->db->schema->getTableSchema('options');

        if ($tableSchema != null) {
            $this->insert('options', [
                'key' => 'email_host',
                'value' => 'smtp.gmail.com',
            ]);

            $this->insert('options', [
                'key' => 'email_port',
                'value' => '465',
            ]);

            $this->insert('options', [
                'key' => 'email_encryption',
                'value' => 'ssl',
            ]);
        }
    }
}
