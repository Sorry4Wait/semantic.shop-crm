<?php

use yii\db\Migration;

/**
 * Class m180605_171303_add_foreign_key_for_table_project
 */
class m180605_171303_add_foreign_key_for_table_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {		
		$this->createIndex(
            'idx-task-project_id',
            'task',
            'project_id'
        );
		
        $this->addForeignKey(
            'fk-task-project_id',
            'task',
            'project_id',
            'project',
            'id',
            'RESTRICT',
			'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropForeignKey(
            'fk-task-project_id',
            'task'
        );
		
		$this->dropIndex(
            'idx-task-project_id',
            'task'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180605_171303_add_foreign_key_for_table_project cannot be reverted.\n";

        return false;
    }
    */
}
