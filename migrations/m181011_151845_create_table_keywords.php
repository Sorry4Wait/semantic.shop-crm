<?php

use yii\db\Migration;

/**
 * Class m181011_151845_create_table_keywords
 */
class m181011_151845_create_table_keywords extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('keywords', [
            'id' => $this->primaryKey(),
            'keyword' => $this->string(),
            'frequency' => $this->integer(),
            'status' => $this->string(),
            'group_id' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('keywords');
    }

}
