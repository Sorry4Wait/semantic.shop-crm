<?php

use yii\db\Migration;

/**
 * Handles adding userId to table `task`.
 */
class m180813_170945_add_userId_column_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'user_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task', 'user_id');
    }
}
