<?php

use yii\db\Migration;

/**
 * Class m181017_072406_alter_table_task
 */
class m181017_072406_alter_table_task extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->alterColumn('task', 'task_text', $this->binary(50000));
    }

    public function down()
    {
        echo "m181017_072406_alter_table_task cannot be reverted.\n";

        return false;
    }

}
