<?php

use yii\db\Migration;

/**
 * Class m181015_100540_add_new_column_to_table_project
 */
class m181015_100540_add_new_column_to_table_project extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('project', 'allowed_groups', $this->integer());
    }

    public function down()
    {
        echo "m181015_100540_add_new_column_to_table_project cannot be reverted.\n";

        return false;
    }

}
