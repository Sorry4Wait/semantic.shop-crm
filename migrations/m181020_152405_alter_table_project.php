<?php

use yii\db\Migration;

/**
 * Class m181020_152405_alter_table_project
 */
class m181020_152405_alter_table_project extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->alterColumn('project', 'tz_binet', $this->string());
    }

    public function down()
    {
        echo "m181020_152405_alter_table_project cannot be reverted.\n";

        return false;
    }

}
