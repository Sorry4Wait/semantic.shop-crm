<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `group`.
 */
class m180608_182349_drop_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->dropForeignKey(
            'fk-task-group_id',
            'task'
        );
		
		$this->dropIndex(
            'idx-task-group_id',
            'task'
        );
		
        $this->dropTable('group');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('group', [
            'id' => $this->primaryKey(),
        ]);
    }
}
