<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m180523_211929_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('task', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
			'status' => $this->string(),
			'project_id' => $this->integer(11),
			'project_TZBinet' => $this->string(),
			'date_add' => $this->datetime(),
			'deadline' => $this->date(),
			'main_keywords' => $this->string(),
			'additional_keywords' => $this->string(),
			'task_text' => $this->string(1500),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}


















