<?php

use yii\db\Migration;

/**
 * Class m180803_095436_create_options_table_exists
 */
class m180803_095436_create_options_table_exists extends Migration
{
    public function safeUp()
    {
        $tableSchema = Yii::$app->db->schema->getTableSchema('options');

        if($tableSchema == null){
            $this->createTable('options', [
                'id' => $this->primaryKey(),
                'key' => $this->string()->notNull()->unique(),
                'value' => $this->string()->notNull(),
            ]);

            $this->insert('options', [
                'key' => 'site_email',
                'value' => 'test@test.com',
            ]);

            $this->insert('options', [
                'key' => 'site_email_name',
                'value' => 'Test',
            ]);
            $this->insert('options', [
                'key' => 'email_host',
                'value' => 'smtp.gmail.com',
            ]);

            $this->insert('options', [
                'key' => 'email_port',
                'value' => '465',
            ]);

            $this->insert('options', [
                'key' => 'email_encryption',
                'value' => 'ssl',
            ]);
            $this->insert('options', [
                'key' => 'site_email_password',
                'value' => 'testsemantic123',
            ]);
        }
    }
}
