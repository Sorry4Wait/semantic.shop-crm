<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Админ. панель', 'options' => ['class' => 'header']],
                    ['label' => 'Проекты', 'icon' => 'book', 'url' => ['project/index']],
                    ['label' => 'ТЗ',
                        'icon' => 'calendar',
                        'items' => [
                            [
                                'label' => 'ТЗ',
                                'encode' => false,
                                'icon' => 'calendar',
                                'url' => ['task/projects-status'],
                                'active' => $this->context->route == 'admin/task/all-tasks' ||
                                    $this->context->route == 'admin/task/projects-status',
                            ],
                            [
                                'label' => 'Исходные ключевики',
                                'icon' => 'calendar-o',
                                'url' => ['task/projects-status', 'status' => '1'],
                                'active' => Yii::$app->request->get('status') == 1 ||
                                    $this->context->route == 'admin/task/status1',

                            ],
                            [
                                'label' => 'В работе',
                                'icon' => 'calendar-o',
                                'url' => ['task/projects-status', 'status' => '4'],
                                'active' => Yii::$app->request->get('status') == 4 ||
                                    $this->context->route == 'admin/task/status4',

                            ],
                            [
                                'label' => 'Готовое ТЗ',
                                'icon' => 'calendar-check-o',
                                'url' => ['task/projects-status', 'status' => '2'],
                                'active' => Yii::$app->request->get('status') == 2 ||
                                    $this->context->route == 'admin/task/status2'
                                    ||
                                    $this->context->route == 'admin/task/index',
                            ],
                            [
                                'label' => 'Использованные ТЗ',
                                'icon' => 'archive',
                                'url' => ['task/projects-status', 'status' => '3'],
                                'active' => Yii::$app->request->get('status') == 3 ||
                                    $this->context->route == 'admin/task/status3',
                            ],
                        ],
                    ],

                    ['label' =>'Ключевики',
                        'icon' => 'calendar',
                        'items' => [
                            [
                                'label' => 'Исходные группы',
                                'url' => ['keyword-groups/source-groups'],
                            ],
                            [
                                'label' => 'В работе',

                                'url' => ['keyword-groups/in-work'],
                            ],
                            [
                                'label' => 'На модерации',

                                'url' => ['keyword-groups/moderation'],
                            ],
                            [
                                'label' => 'Обработка групп',

                                'url' => ['keyword-groups/group-processing',],
                            ],
                            [
                                'label' => 'Проверка групп',

                                'url' => ['keyword-groups/group-check'],
                            ],
                            [
                                'label' => 'Готовые группы',

                                'url' => ['keyword-groups/final-group'],
                            ],
                            [
                                'label' => 'Использованные',

                                'url' => ['keyword-groups/used-group'],
                            ],

                        ],
                    ],

                    ['label' => 'Отчет', 'icon' => 'book', 'url' => ['report/index']],

                    ['label' => 'Пользователи', 'icon' => 'user-o', 'url' => ['user/index']],
                    [
                        'label' => 'Настройки',
                        'icon' => 'fa fa-cog',
                        'url' => ['options/index',],
                    ],
                ],
            ]

        ) ?>

    </section>

</aside>

















