<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\TzBinetConnect */

$this->title = 'Create Tz Binet Connect';
$this->params['breadcrumbs'][] = ['label' => 'Tz Binet Connects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tz-binet-connect-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
