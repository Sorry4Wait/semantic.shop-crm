<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Project;
use app\modules\admin\models\Group;
use kartik\select2\Select2;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">

        <div class="row">

            <div class="col-md-4">

                <?php $projects = Project::find()->orderBy('name')->all(); ?>

                <?=
                $form->field($model, 'project_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::Map($projects, 'id', 'name'),
                    'options' => [
                        //'value' =>  $model->type,
                        'placeholder' => 'Выберите предприятие',
                        'onchange'=>'
                            $.post( "/admin/task/lists?id='.'"+$(this).val(), function( data ){
                                $( "select#task-project_tzbinet" ).html( data);
                            });
                            
                        '
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-4">

                <?= $form->field($model, 'project_TZBinet')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getEquipmentList($model->project_id),//\app\models\Equipment::getList(),
                    'options' => [
                        'placeholder' => 'Выберите Проект',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'status')->dropDownList($model->statuses); ?>
            </div>
        </div>

        <br>
        <?= $form->field($model, 'main_keywords')->textArea(['maxlength' => true, 'rows' => 10]) ?>

        <?= $form->field($model, 'additional_keywords')->textArea(['maxlength' => true, 'rows' => 10]) ?>

        <?= $form->field($model, 'task_text', ['errorOptions' => ['encode' => false]])->textArea(['maxlength' => true, 'rows' => 20]) ?>

        <?php if ($model->task_text) {
            ?>
            <label class="control-label" for="task-missed">Потерянные ключевики</label>
            <?= Html::textArea('task-missed', $model->getMissedKeywords(), [
                'label' => 'Потерянные ключевики',
                'template' => "{label}\n{input}\n{hint}\n{error}",
                'id' => 'keywords',
                'rows' => 15,
                'style' => 'width:100%',
                'disabled' => true
            ]) ?>

        <?php } ?>

        <?= $form->field($model, 'group_id')->textInput() ?>

		<?= $form->field($model, 'import_key')->dropDownList($model->importKey, ['prompt' => 'Выберите значение...']); ?>

        <div class="row">

            <div class="col-md-2">
                <?=
                $form->field($model, 'date_add')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy H:i:s',
                    'options' => ['class' => 'form-control', 'disabled' => true],
                ])
                ?>
            </div>
            <div class="col-md-2">
                <?=
                $form->field($model, 'deadline')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'yyyy.MM.dd',
                    'options' => ['class' => 'form-control'],
                ])
                ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true]) ?>
            </div>
			 <div class="col-md-2">
                <?= $form->field($model, 'fire')->dropDownList([0 => 'Нет', 1 => 'Да']); ?>
            </div>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Записать', ['class' => 'btn btn-success']) ?>

        <?php if(Yii::$app->request->get('id')): ?>

            <input type="hidden" id="model_id" name="model_id" value=<?=$model->id?>>
            <span class="btn btn-info" onClick="to_moderation()"><i class="fa fa-close"></i> Отправить на доработку</span>

        <?php endif;?>
    </div>



    <?php ActiveForm::end(); ?>

</div>

<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string) "task-{$model->id}",// type and id
    'showDeleted' => false,
]);?>
<script type="text/javascript">


    function to_moderation() {

        var keys = $('#model_id').val();

        $.ajax({
            type: "POST",
            url: 'to-moderation',
            data: {model_id: keys},
            success: function (result) {
                swal({
                    title: "",
                    text: result,
                    confirmButtonColor: "#337ab7"
                });
                $.pjax.reload({container: '#pjax_grid'});
            }
        });

    }


</script>





















