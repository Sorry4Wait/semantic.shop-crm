<?php

use yii\db\Query;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы ключевиков';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keyword-groups-index">



    <span class="btn btn-success" onClick="to_accept()"><i class="fa fa-check"></i> Принять</span>
    <span class="btn btn-info" onClick="to_moderation()"><i class="fa fa-close"></i> На доработку</span>

    <p>

    </p>
    <div class="panel panel-default panel-body">



        <?php Pjax::begin(['id' => 'pjax_grid']); ?>

        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function($model) {
                        return ['value' => $model->id];
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'attribute' => 'comments',
                    'value' => function ($model){
                        $string = null;
                        $query = new Query;

                        $query->select('*')
                            ->from('comment')
                            ->where(['entity' => (string)"group_id-{$model->id}"]);
                        $rows = $query->all();
                        if($rows){
                            foreach ($rows as $q){
                                $string = $string . $q['text'];
                            }
                        }else{
                            $string = 'Без коментарии';
                        }

                        return $string;
                    },
                     'contentOptions' => ['style' => 'width: 20%;white-space: nowrap;'],
                ],
                [
                    'attribute' => 'name',
                    'content' => function($model){
                        return  Html::a($model->name, ['update', 'id' => $model->id]);
                    },
                    'contentOptions' => ['style' => 'width: 20%;white-space: nowrap;'],
                    'filter' => false,
                ],
                [
                    'attribute' => 'keys',
                    'value' => function ($model){
                            $string = null;
                            $keys = \app\modules\admin\models\Keywords::findAll(['group_id' => $model->connect_id]);

                            foreach ($keys as $key){
                                if($key->color){
                                    $code = \app\modules\admin\models\Colors::findOne(['id' => $key->color]);
                                    $str = '<span style="color:'.$code->code.'">'.$key->keyword.'</span>'.'<br />';
                                }else{
                                    $str = "<span>".$key->keyword."</span>".'<br />';
                                }

                                $string = $string . $str;
                            }
                        return $string;

                    },

                    'format' => 'raw',
                    'filter' => true,

                ],
                [
                    'attribute' => 'frequency',
                    'filter' => false,
                ],
                [
                    'attribute' => 'semantik',
                    'value' => function ($model){
                            $user = \app\modules\admin\models\User::findOne(['id' => $model->semantik]);
                            return $user->username;
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'in_work',
                    'filter' => false,
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>


<script type="text/javascript">


    function to_moderation() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Отправить на доработку?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-moderation',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_accept() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Принять выбранные группы?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-accept',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

</script>


