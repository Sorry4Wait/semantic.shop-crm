<?php

use app\modules\admin\models\Project;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\module\Comments;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\KeywordGroups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keyword-groups-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default panel-body">

        <div class="row">

            <div class="col-md-4">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'frequency')->textInput() ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'main_keywords')->textArea(['maxlength' => true, 'rows' => 10])  ?>
            </div>
            <div class="col-md-12">
    <?= $form->field($model, 'add_keywords')->textArea(['maxlength' => true, 'rows' => 10])  ?>
            </div>
            <div class="col-md-4">
    <?php $projects = Project::find()->orderBy('name')->all(); ?>

    <?=
    $form->field($model, 'project')->widget(Select2::classname(), [
        'data' => ArrayHelper::Map($projects, 'id', 'name'),
        'options' => ['placeholder' => 'Выберите проект...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'status')->dropDownList($model->statuses); ?>
            </div>

            <div class="col-md-4">
    <?= $form->field($model, 'import_key')->dropDownList($model->importKey, ['prompt' => 'Выберите значение...']); ?>
            </div>
            </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <input type="hidden" id="model_id" name="model_id" value=<?=$model->id?>>
    <span class="btn btn-info" onClick="to_moderation()"><i class="fa fa-close"></i> На доработку</span>

    <?php ActiveForm::end(); ?>

</div>
<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string)"group_id-{$model->id}", // type and id
]); ?>
<script type="text/javascript">


    function to_moderation() {

        var keys = $('#model_id').val();

            $.ajax({
                type: "POST",
                url: 'to-moderation',
                data: {model_id: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });

    }


</script>

