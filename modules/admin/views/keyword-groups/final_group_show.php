
<?php

use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\controllers\KeywordGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keyword-groups-index">



    <span class="btn btn-danger" onClick="deleteSelected()"><i class="fa fa-trash-o"></i> Удалить</span>
    <span class="btn btn-info" onClick="to_late()"><i class="fa fa-close"></i> На потом</span>
    <input type="hidden" id="project-project" value=<?=$project->id?>>

    <?php if($project->tz_binet == \app\modules\admin\models\Project::BINET_MULTIPLE):?>
    <?php
    \yii\bootstrap\Modal::begin([
        'header' => 'Проекты в TZ Binet',
        'toggleButton' => ['label' => '<i class="fa fa-check"></i> Подготовить ТЗ',
            'tag' => 'button',
            'class' => 'btn btn-success',],
    ]);
    ?>
        <?php $form = ActiveForm::begin(['action' => 'group']); ?>


        <?php $projects = \app\modules\admin\models\TzBinetConnect::findAll(['project_id' => $project->id]); ?>

        <?=
        $form->field($project, 'binet')->widget(Select2::classname(), [
            'data' => ArrayHelper::Map($projects, 'name', 'name'),
            'options' => ['placeholder' => 'Выберите проект TZ Binet'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(false);
        ?>



        <div class="form-group">
            <span class="btn btn-success" onClick="select1()">Создать</span>
        </div>

        <?php ActiveForm::end(); ?>




    <?php \yii\bootstrap\Modal::end(); ?>
    <?php endif;?>


    <?php if($project->tz_binet == \app\modules\admin\models\Project::BINET_ONE):?>
        <?php $id = \app\modules\admin\models\TzBinetConnect::findOne(['project_id' => $project->id]);?>
        <input type="hidden" id="project-tz-binet" value=<?=$id->name?>>
        <span class="btn btn-success" onClick="select2()"><i class="fa fa-check"></i> Подготовить ТЗ</span>
    <?php endif;?>



    <?php if($project->tz_binet == null):?>
        <?php
        \yii\bootstrap\Modal::begin([
            'header' => 'Проект в TZ Binet',
            'toggleButton' => ['label' => '<i class="fa fa-check"></i> Подготовить ТЗ',
                'tag' => 'button',
                'class' => 'btn btn-success',],
        ]);
        ?>
        <?php $form = ActiveForm::begin(['action' => 'group']); ?>


        <?php $projects = \app\modules\admin\models\KeywordGroups::find()->orderBy('name')->all(); ?>


        <?= $form->field($project, 'tz_binet')->textInput(['maxlength' => true])->label(false) ?>



        <div class="form-group">
            <span class="btn btn-success" onClick="select3()">Создать</span>

        </div>

        <?php ActiveForm::end(); ?>




        <?php \yii\bootstrap\Modal::end(); ?>
    <?php endif;?>


    <p>
    </p>
    <div class="panel panel-default panel-body">



        <?php Pjax::begin(['id' => 'pjax_grid']); ?>

        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'checked',
                    'checkboxOptions' => function($model) {
                        return ['value' => $model->id];
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'attribute' => 'name',
                    'content' => function($model){
                        return  Html::a($model->name, ['update', 'id' => $model->id]);
                    },
                    'contentOptions' => ['style' => 'width: 20%;white-space: nowrap;'],
                    'filter' => false,
                ],
                [
                    'attribute' => 'semantik',
                    'value' => function ($model){
                        $user = \app\modules\admin\models\User::findOne(['id' => $model->semantik]);
                        return $user->username;
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'frequency',
                    'filter' => false,
                ],
                [
                    'attribute' => 'keys',
                    'value' => function ($model){

                        return $model->getCountKeywords();

                    },

                    'format' => 'raw',
                    'filter' => true,

                ],
                [
                    'attribute' => 'import_key',
                    'value' => function($searchModel){
                        $a = $searchModel->getImportKey();
                        return $a[$searchModel->import_key];
                    },
                    'filterInputOptions' => [
                        'class' => 'form-control',
                        'prompt' => 'Все',
                    ],
                    'filter'=> array(
                        '1' => 'НЧ',
                        '2' => 'ВЧ',
                        '3' => 'На потом',
                    ),

                ],
                [
                    'attribute' => 'project',
                    'value' => function($searchModel){
                        $project = \app\modules\admin\models\Project::findOne(['id' => $searchModel->project]);
                        return $project->name;
                    },
                    'filter' => false,

                ],
                [
                    'attribute' => 'status',
                    'value' => function($searchModel){
                        $a = \app\modules\admin\models\KeywordGroups::getStatusesLabel();
                        return $a[$searchModel->status];
                    },
                    'filter' => false,

                ],
                [
                    'attribute' => 'in_work',
                    'filter' => false,
                ],

//
//
//                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>


<script type="text/javascript">


    function deleteSelected() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Удалить выбранные Группы?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'delete-selected',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function to_late() {

        var keys = $('#grid').yiiGridView('getSelectedRows');

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('На потом?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'to-late',
                data: {keylist: keys},
                success: function (result) {
                    swal({
                        title: "",
                        text: result,
                        confirmButtonColor: "#337ab7"
                    });
                    $.pjax.reload({container: '#pjax_grid'});
                }
            });
        }
    }

    function select1() {
        $('form').submit(function (e) {
            e.preventDefault();
        });
        var binet = $('#project-binet').val();
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var project = $('#project-project').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        if(binet == 0){
            swal({
                title: "",
                text: 'Нужно указать проект в TZ Binet',
                confirmButtonColor: "#337ab7"
            });
            return;
        }


        var dialog = confirm('Создать ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'check',
                data: {keylist: keys,binet:binet,project:project},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }
    function select2() {

        var binet = $('#project-tz-binet').val();
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var project = $('#project-project').val();

        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        var dialog = confirm('Создать ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'check',
                data: {keylist: keys,binet:binet,project:project},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

    function select3() {

        var binet = $('#project-tz_binet').val();
        var keys = $('#grid').yiiGridView('getSelectedRows');
        var project = $('#project-project').val();
        var create = "create";
        if (keys == '') {
            swal({
                title: "",
                text: 'Отметьте флажками необходимые Группы',
                confirmButtonColor: "#337ab7"
            });
            return;
        }

        if (binet == '') {
            swal({
                title: "",
                text: 'Нужно указать проект в TZ Binet',
                confirmButtonColor: "#337ab7"
            });
            return;
        }
        var dialog = confirm('Создать ТЗ?');

        if (dialog == true) {

            $.ajax({
                type: "POST",
                url: 'check',
                data: {keylist: keys,binet:binet,project:project,create:create},
                success: function (result) {
//                    swal({
//                        title: "",
//                        text: result,
//                        confirmButtonColor: "#337ab7"
//                    });
                    location.reload();
                }
            });
        }
    }

</script>


