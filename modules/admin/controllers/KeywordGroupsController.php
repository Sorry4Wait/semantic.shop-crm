<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Keywords;
use app\modules\admin\models\Project;
use app\modules\admin\models\ProjectSearch;
use app\modules\admin\models\Task;
use app\modules\admin\models\TzBinetConnect;
use Yii;
use app\modules\admin\models\KeywordGroups;
use app\modules\admin\controllers\KeywordGroupsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KeywordGroupsController implements the CRUD actions for KeywordGroups model.
 */
class KeywordGroupsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Displays a single KeywordGroups model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionSourceGroups()
    {
        $status = KeywordGroups::STATUS_ISXODNIE_GRUPPI;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('source_groups', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionSourceGroupsShow()
    {
        $status = KeywordGroups::STATUS_ISXODNIE_GRUPPI;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('source_groups_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInWork()
    {
        $status = KeywordGroups::STATUS_V_RABOTE;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('in_work', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionInWorkShow()
    {
        $status = KeywordGroups::STATUS_V_RABOTE;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('in_work_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionModeration()
    {
        $status = KeywordGroups::STATUS_NA_MODERATSII;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('moderation', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);

    }

    public function actionModerationShow()
    {
        $status = KeywordGroups::STATUS_NA_MODERATSII;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('moderation_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFinalGroupShow()
    {
        $status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
        $project = Project::findOne(['id' => $_GET['project_id']]);
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('final_group_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project' => $project,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }


    public function actionFinalGroup()
    {

        $status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('final_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroupProcessingShow()
    {
        $status = KeywordGroups::STATUS_OBRABOTKA_GRUPP;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('group_processing_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }


    public function actionGroupProcessing()
    {

        $status = KeywordGroups::STATUS_OBRABOTKA_GRUPP;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('group_processing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionUsedGroupShow()
    {
        $status = KeywordGroups::STATUS_ISPOLZOVANNIE;
        $model = new KeywordGroups();
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('used_group_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }


    public function actionUsedGroup()
    {

        $status = KeywordGroups::STATUS_ISPOLZOVANNIE;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('used_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }


    public function actionGroupCheckShow()
    {
        $status = KeywordGroups::STATUS_PROVERKA_GRUPP;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->query->andWhere(['project' => $_GET['project_id']]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('group_check_show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }


    public function actionGroupCheck()
    {

        $status = KeywordGroups::STATUS_PROVERKA_GRUPP;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('group_check', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }


    public function actionToNewProject()
    {

        foreach ($_POST['keylist'] as $key){
            $group = KeywordGroups::findOne(['id' => $key]);
            $group->project = $_POST['id'];
            $group->status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
            $group->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Экспортирование завершено успешно!');
        return true;
    }

    /**
     * Creates a new KeywordGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KeywordGroups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KeywordGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KeywordGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KeywordGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KeywordGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KeywordGroups::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionControl($id)
    {
        $group = KeywordGroups::findOne(['id' => $id]);
        $keywords = Keywords::findAll(['group_id' => $group->connect_id]);
        $searchModel = new KeywordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['group_id'=> $group->connect_id]);
        return $this->render('control', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'keywords' => $keywords,
            'model' => $group,
        ]);


    }

    public function actionCheck()
    {

        if(isset($_POST['create'])){
            $m = new TzBinetConnect();
            $m->project_id = $_POST['project'];
            $m->name = $_POST['binet'];
            $m->save();
        }



        foreach ($_POST['keylist'] as $key){
            $model = KeywordGroups::findOne(['id' => $key]);
            $task = new Task();
            $task->status = Task::STATUS_SOURCE_KEYS;
            $task->project_id = $model->project;
            $task->project_TZBinet = $_POST['binet'];
            $task->name = $model->name;
            $task->date_add =  date('Y-m-d H:i:s');


            $keys = Keywords::findAll(['group_id' => $model->connect_id]);
            $with_out_space = " ";
            $with_space = " ";
            $str = "";
            $count = 0;
            $fre = 0;
            foreach ($keys as $k){
                $count++;
                if($count <= 5){
                    $fre += $k->frequency;
                    $with_space     = $with_space  . "  " .$k->keyword;
                    $with_out_space = $with_out_space.$k->keyword;

                }else{
                    $str = $str." ".$k->keyword;
                }

            }

            if($fre >= 100){
                $task->main_keywords = $with_space . $str;
            }else{
                $task->main_keywords = $with_out_space . $str;
            }



            $task->additional_keywords = $model->add_keywords;
            $task->group_id = $model->connect_id;
            $task->import_key = $model->import_key;
            $task->user_id = $model->semantik;



            $model->status = KeywordGroups::STATUS_ISPOLZOVANNIE;
            $model->save();
            $task->save(false);
        }

        return 'Группы в статусе Использована';


    }


    public function actionToLate()
    {

        foreach ($_POST['keylist'] as $key){
            $model = KeywordGroups::findOne(['id' => $key]);
            $model->import_key = 3;
            $model->save();
        }

        return 'Группе присвоин метка «На потом»!';
    }



    public function actionDeleteSelected()
    {
        foreach ($_POST['keylist'] as $key) {

            $model = KeywordGroups::findOne(['id' => $key]);
            $keys = Keywords::findAll(['group_id' => $model->connect_id]);

            foreach ($keys as $k){
                $k->status = 0;
                $k->group_id = 0;
                $k->save();

            }

            $model->delete();

        }
        return 'Группы удалены!';
    }

    public function actionToModeration()
    {
        if(isset($_POST['model_id'])){
            $model = KeywordGroups::findOne(['id' => $_POST['model_id']]);
            $model->status--;
            $model->save();
            Yii::$app->getSession()->setFlash('success', 'Группа отправлень на доработку!');
            return $this->redirect('moderation');
        }

        foreach ($_POST['keylist'] as $key){
            $model = KeywordGroups::findOne(['id' => $key]);
            $model->status--;
            $model->save();
        }

        return 'Группы отправлены на модерации!';
    }

    public function actionToAccept()
    {

        foreach ($_POST['keylist'] as $key){
            $model = KeywordGroups::findOne(['id' => $key]);
            $model->status++;
            if($model->status == KeywordGroups::STATUS_GOTOVIE_GRUPPI)
                $model->accept_time =  date('Y-m-d H:i:s');
            $model->save();
        }

        return 'Выбранные группы приняты!';
    }

}
