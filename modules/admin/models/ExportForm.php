<?php

namespace app\modules\admin\models;

use yii\base\Model;

/**
 * Class ExportForm
 * @package app\modules\admin\models
 *
 * @property string $taskIdListInputString
 */
class ExportForm extends Model
{

    public $projectId;

    public $tasksIdList;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['projectId', 'taskIdListInputString'], 'required'],
        ];
    }

    public function getTaskIdListInputString()
    {
        return implode('#', $this->tasksIdList);
    }

    public function setTaskIdListInputString($list)
    {
        $this->tasksIdList = explode('#', $list);
    }

    public function attributeLabels()
    {
        return [
            'projectId' => 'Проект',
        ];
    }

    public function export()
    {
        return true;
    }
}
