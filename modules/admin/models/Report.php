<?php

namespace app\modules\admin\models;

use yii\base\Model;

/**
 * Class Report
 * @package app\modules\admin\models
 *
 * @property-read int $doneCount
 */
class Report extends Model
{
    /**
     * @var User
     */
    public $user = false;

    /**
     * @var Period
     */
    public $period = false;

    private $_doneCount = null;

    /**
     * @return int
     */
    public function getDoneCount()
    {
        if (is_null($this->_doneCount)) {
            $this->_doneCount = $this->getDoneCountFromDB();
        }

        return $this->_doneCount;
    }

    /**
     * @return int
     */
    protected function getDoneCountFromDB()
    {
        $query = Task::find()
            ->where([
                'status' => Task::STATUS_USED,
            ]);

        if ($this->user) {
            $query->andWhere([
                'user_id' => $this->user->id
            ]);
        }

        if ($this->period) {
            $query->andWhere([
                'between', 'date_add', $this->period->start, $this->period->end
            ]);
        }

        return intval($query->count());
    }
}



