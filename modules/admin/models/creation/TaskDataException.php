<?php

namespace app\modules\admin\models\creation;


use yii\base\Exception;

class TaskDataException extends Exception
{
    public $attribute = 'keywords';
}