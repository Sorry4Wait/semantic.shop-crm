<?php

namespace app\modules\admin\models\creation;


use app\modules\admin\models\Project;
use app\modules\admin\models\Task;
use yii\base\BaseObject;

class TaskDataArrayBuilder extends BaseObject
{
    /**
     * @var string
     */
    public $keywords;

    /**
     * @var Project
     */
    protected $siteProject = null;
    /**
     * @var string
     */
    protected $binetProjectName = null;

    /**
     * @return string
     */
    public function getBinetProjectName()
    {
        return $this->binetProjectName;
    }

    /**
     * @throws TaskDataException
     * @return []
     */
    public function build()
    {
        $projects = $this->createProjects();

        $keywordsText = $this->clearKeywordsFromProjectNames($this->keywords, $projects);

        $projects = $this->clearProjects($projects);


        $this->binetProjectName = $projects[SpecialCharacter::PROJECT_BINET];
        $this->siteProject = $this->getFindProject($projects[SpecialCharacter::PROJECT_SITE]);


        /**
         * @var string
         */
        $keywords = $this->normalizeKeywords($keywordsText);


        $tasksData = $this->splitToTasks($keywords);

        $tasksData = SpecialCharacter::restoreSpecial($tasksData);

        $this->checkForUniqueAll($tasksData);

        return array_values($tasksData);
    }

    /**
     * @return []
     * @throws TaskDataException
     */
    public function createProjects()
    {
        preg_match_all('/(\n|^)Проект_(.*): [^\n]{1,}/', $this->keywords, $matches);

        if (!$matches[0]) {
            $this->throwException('Проект для данного ТЗ не обнаружен');
        }

        $projects = [];
        foreach ($matches[0] as $key => $match) {
            if (is_int(stripos($match, SpecialCharacter::PROJECT_SITE))) {
                $projects[SpecialCharacter::PROJECT_SITE] = trim($match);
            }
            if (is_int(stripos($match, SpecialCharacter::PROJECT_BINET))) {
                $projects[SpecialCharacter::PROJECT_BINET] = trim($match);
            }
        }

        if (count($projects) < 2) {
            $this->throwException('Проект для данного ТЗ не обнаружен');
        }

        return $projects;
    }

    /**
     * @param $text
     * @param string $attribute
     * @throws TaskDataException
     */
    protected function throwException($text, $attribute = 'keywords')
    {
        $e = new TaskDataException($text);
        $e->attribute = $attribute;
        throw $e;
    }

    public function clearKeywordsFromProjectNames($keywords, $projects)
    {
        $keywordsText = trim($keywords);

        //убрать служебные слова
        foreach ($projects as &$project) {
            $keywordsText = str_ireplace($project, '', $keywordsText);
        }
        return $keywordsText;
    }

    /**
     * @param string[] $projects
     * @return string[]
     */
    public function clearProjects($projects)
    {
        //убрать служебные слова
        foreach ($projects as &$project) {
            $project = trim(str_ireplace(SpecialCharacter::PROJECT_BINET, '', $project));
            $project = trim(str_ireplace(SpecialCharacter::PROJECT_SITE, '', $project));
        }
        return $projects;
    }

    /**
     * @param $name
     * @return Project
     * @throws TaskDataException
     */
    public function getFindProject($name)
    {
        /**
         * @var Project $projectSite
         */
        $projectSite = Project::findOne(['name' => $name]);
        if (!$projectSite) {
            $this->throwException('Проект "' . $name . '" не найден в системе.');
        }
        return $projectSite;
    }

    public function normalizeKeywords($keywords)
    {
        $strings = $this->prepareKeywordsStrings($keywords);

        $strings = $this->replaceSpecial($strings);

        $strings = $this->normalizeStrings($strings);

        // возвращаем все в $keywords, но с нашими правками
        $keywords = implode(PHP_EOL, $strings);

        return $keywords;
    }

    /**
     * @param $keywords
     * @return string[]
     */
    public function prepareKeywordsStrings($keywords)
    {
        // разбиваем все на отдельные строки
        $strings = explode(PHP_EOL, $keywords);

        $strings = SpecialCharacter::replaceSpecial($strings);

        $strings = implode(PHP_EOL, $strings);

        foreach ($this->getBadWords() as $badWord) {
            $strings = str_replace(PHP_EOL . $badWord, $badWord, $strings);
        }

        $strings = explode(PHP_EOL, $strings);

        return $strings;
    }

    protected function getBadWords()
    {
        return SpecialCharacter::getBadWords();
    }

    /**
     * @param $strings
     * @return string[]
     */
    public function replaceSpecial($strings)
    {
        foreach ($strings as $key => &$string) {
            // пустая строка является разделителем между разными ТЗ
            if (empty(trim($string))) {
                $string = SpecialCharacter::SPACE;
            }
            // '+' является разделителем между разными ТЗ
            if (trim($string) == '+') {
                $string = SpecialCharacter::SPACE;
            }
            // ! является разделителем между основными и доп. ключевиками
            if (trim($string) == '!') {
                $string = SpecialCharacter::BANG;
            }

            $string = trim($string);
        }
        return $strings;
    }

    /**
     * @param string[] $strings
     */
    public function normalizeStrings($strings)
    {
        foreach ($strings as $key => $string) {
            if ($string == SpecialCharacter::TVCH || $string == SpecialCharacter::TDX || in_array($string, $this->getBadWords())) {
                if ($key == (count($strings) - 1) && !in_array($strings[$key + 1], [SpecialCharacter::BANG, SpecialCharacter::SPACE])) {
                    $strings[$key - 1] = $strings[$key - 1] . $string;
                    $strings[$key] = '';
                } else {
                    $strings[$key + 1] = $string . $strings[$key + 1];
                    $strings[$key] = '';
                }

            }
        }

        return array_diff($strings, ['']);
    }

    /**
     * @param $keywords
     * @return string[]
     */
    public function splitToTasks($keywords)
    {
        // разбиваем на разные ТЗ
        $arr_tasks = explode(SpecialCharacter::SPACE . PHP_EOL, $keywords);
        //чистим
        $arr_tasks = array_filter($arr_tasks);
        return $arr_tasks;
    }

    /**
     * @param $tasksData
     * @throws TaskDataException
     */
    public function checkForUniqueAll($tasksData)
    {
        foreach ($tasksData as $taskString) {
            $taskKeywords = explode(SpecialCharacter::BANG, $taskString);

            $this->checkForUnique($taskKeywords[0], $this->getProject());
        }
    }

    /**
     * @param $keywords
     * @param Project $project
     * @throws TaskDataException
     */
    public function checkForUnique($keywords, $project)
    {
        $strings = explode(PHP_EOL, $keywords);

        $name = null;

        if (isset($strings[0]) && !is_numeric(trim($strings[0]))) {
            $name = trim($strings[0]);
        } elseif (isset($strings[1]) && !is_numeric(trim($strings[1]))) {
            $name = trim($strings[1]);
        }

        $similarTaskCount = $this->getSimilarTaskCount($name, $project);

        if ($similarTaskCount > 0) {
            $this->throwException("Ключевое слово '{$name}' уже используется в другом тз данного проекта.");
        }
    }

    public function getSimilarTaskCount($name, $project)
    {
        return $similarTaskCount = Task::find()
            ->where(['name' => $name])
            ->andWhere(['project_id' => $project->id])
            ->count();
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->siteProject;
    }
}