<?php

namespace app\modules\admin\models\creation;


use app\modules\admin\models\Task;
use yii\base\BaseObject;

class TaskBuilder extends BaseObject
{
    public $projectId;
    public $projectTZBinet;
    public $taskText;

    public function build()
    {
        $task = new Task();
        $task->status = Task::STATUS_SOURCE_KEYS;
        $task->project_id = $this->projectId;
        $task->project_TZBinet = $this->projectTZBinet;

        $tdx = '';

        $taskText = $this->taskText;

        if (is_int(stripos($taskText, SpecialCharacter::TDX))) {
            $taskText = str_ireplace(SpecialCharacter::TDX, '', $taskText);
            $tdx = SpecialCharacter::TDX;
        }

        $tvch = '';

        if (is_int(stripos($taskText, SpecialCharacter::TVCH))) {
            $taskText = str_ireplace(SpecialCharacter::TVCH, '', $taskText);
            $tvch = SpecialCharacter::TVCH;
        }

        // разбиваем на осн. и доп. ключевики
        $keywords = explode(SpecialCharacter::BANG, $taskText);

        $keywords = SpecialCharacter::replaceSpecial($keywords);

        $mainKeywords = isset($keywords[0]) ?
            $keywords[0] :
            '';

        $addKeywords = isset($keywords[1]) ?
            $keywords[1] . PHP_EOL . $tvch . PHP_EOL . $tdx :
            '';


        $task->main_keywords = SpecialCharacter::restoreSpecial($mainKeywords);
        $task->additional_keywords = SpecialCharacter::restoreSpecial($addKeywords);

        return $task;
    }

}