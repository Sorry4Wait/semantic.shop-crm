<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "keywords".
 *
 * @property int $id
 * @property string $keyword
 * @property int $frequency
 * @property string $status
 * @property int $group_id
 */
class Keywords extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'keywords';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['frequency', 'group_id'], 'integer'],
            [['keyword', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'keyword' => 'Ключевик',
            'frequency' => 'Частотность',
            'status' => 'Статус',
            'group_id' => 'ИД группы',
        ];
    }
}
