<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "keyword_groups".
 *
 * @property int $id
 * @property string $name
 * @property int $semantik
 * @property int $frequency
 * @property string $add_keywords
 * @property int $project
 * @property int $status
 * @property int $import_key
 * @property string $in_work
 */
class KeywordGroups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const STATUS_ISXODNIE_GRUPPI = '1';
    const STATUS_V_RABOTE = '2';
    const STATUS_NA_MODERATSII = '3';
    const STATUS_OBRABOTKA_GRUPP = '4';
    const STATUS_PROVERKA_GRUPP = '5';
    const STATUS_GOTOVIE_GRUPPI = '6';
    const STATUS_ISPOLZOVANNIE = '7';

    public $conn_id;
    public $keys;
    public $keywords_count;
    public $comments;
    public static function tableName()
    {
        return 'keyword_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semantik', 'frequency', 'project', 'status', 'import_key'], 'integer'],
            [['in_work'], 'safe'],
            [['name', 'add_keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Название группы',
            'semantik' => 'Семантик',
            'frequency' => 'Частотность',
            'add_keywords' => 'Дополнительные ключевики',
            'project' => 'Проект',
            'status' => 'Статус',
            'import_key' => 'Метка',
            'in_work' => 'Взято в работу',
            'keywords_count' => 'Кол-во ключевиков',
            'conn_id' => 'Группы',
            'keys' => 'Ключевик',
            'comments' => 'Коментарии',
            'main_keywords' => 'Основные ключевики'
        ];
    }



    public function getStatuses()
    {
        return self::getStatusesLabel();
    }

    public static function getStatusesLabel()
    {
        return [

            self::STATUS_ISXODNIE_GRUPPI => 'Исходные группы',
            self::STATUS_V_RABOTE => 'В работе',
            self::STATUS_NA_MODERATSII=> 'На модерации',
            self::STATUS_OBRABOTKA_GRUPP => 'Обработка групп',
            self::STATUS_PROVERKA_GRUPP => 'Проверка групп',
            self::STATUS_GOTOVIE_GRUPPI => 'Готовые группы',
            self::STATUS_ISPOLZOVANNIE  => 'Использованные',
        ];
    }

    public function getImportKey()
    {
        return array(
            '1' => 'НЧ',
            '2' => 'ВЧ',
            '3' => 'На потом',

        );
    }

    public function getUserName($id)
    {
        return User::find()->where(['id' => $id])->one()['username'];
    }

    public static function getStatusName($id)
    {
        $array = self::getStatusesLabel();

        return $array[$id];
    }


    public function getTasksCountByStatus($status = false)
    {
        $count = (new Query())
            ->select('keyword_groups.id')
            ->from('keyword_groups')
            ->where(['project' => $this->id]);

        if ($status) {
            $count->andWhere(['status' => $status]);
        }

        return $count->count();
    }

    public function getCountKeywords()
    {
        $count = (new Query())
            ->select('keywords.id')
            ->from('keywords')
            ->where(['group_id' => $this->connect_id]);

        return $count->count();
    }

    public static function GetProjects(){
        $projects = Project::find()->orderBy('name')->all();
        return ArrayHelper::Map($projects, 'id', 'name');
    }

    public static function GetUsers(){
        $projects = User::find()->where(['role' => 'semantic'])->orderBy('username')->all();
        return ArrayHelper::Map($projects, 'id', 'username');
    }
}
