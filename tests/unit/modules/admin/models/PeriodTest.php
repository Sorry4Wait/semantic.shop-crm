<?php

namespace tests\modules\admin\models;

use app\modules\admin\models\Period;

class PeriodTest extends \Codeception\Test\Unit
{
    public function testCreate()
    {
        $period = new Period();
        expect($period)->isInstanceOf(Period::class);
    }

    public function testCreateFromDataRange()
    {
        $text = '2018-09-14 - 2018-09-20';
        $period = Period::fromDataRange($text);

        expect($period->start)->equals('2018-09-14');
        expect($period->end)->equals('2018-09-20');
    }

}
