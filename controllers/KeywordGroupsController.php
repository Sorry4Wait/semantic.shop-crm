<?php

namespace app\controllers;

use app\modules\admin\controllers\KeywordsSearch;
use app\modules\admin\models\Keywords;
use app\modules\admin\models\ProjectSearch;
use Yii;
use app\modules\admin\models\KeywordGroups;
use app\modules\admin\controllers\KeywordGroupsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KeywordGroupsController implements the CRUD actions for KeywordGroups model.
 */
class KeywordGroupsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KeywordGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['semantik'=> null]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KeywordGroups model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionShow()
    {
        $status = Yii::$app->request->get('status');
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status'=> $status]);
        $dataProvider->query->andWhere(['semantik'=> Yii::$app->user->getId()]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new KeywordGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KeywordGroups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KeywordGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['keyword-groups/control','id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KeywordGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KeywordGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KeywordGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KeywordGroups::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionWork()
    {
        if (Yii::$app->request->post('keylist')) {
            foreach(Yii::$app->request->post('keylist') as $id){
                $group = KeywordGroups::findOne(['id' => (int)$id]);
                $group->status = KeywordGroups::STATUS_V_RABOTE;
                $group->semantik = Yii::$app->user->id;
                $group->in_work =  date('Y-m-d H:i:s');
                $group->save(false);
            }
        }

        return 'Ключевики взялись на работу.';
    }

    public function actionControl($id)
    {
            $group = KeywordGroups::findOne(['id' => $id]);
            $keywords = Keywords::findAll(['group_id' => $group->connect_id]);
            $searchModel = new KeywordsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['group_id'=> $group->connect_id]);
            return $this->render('control', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'keywords' => $keywords,
                'model' => $group,
            ]);


    }

    public function actionCheck()
    {

        $post = Yii::$app->request->post('KeywordGroups');
      $model = KeywordGroups::findOne(['id' => $post['id']]);
      $model->add_keywords = $post['add_keywords'];
      $model->status++;
      $model->save();
        return $this->redirect(['keyword-groups/show','status' =>  $model->status]);


    }

    public function actionGroupProcess($id)
    {
        $group = KeywordGroups::findOne(['id' => $id]);
        $keywords = Keywords::findAll(['group_id' => $group->connect_id]);
        $searchModel = new KeywordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['group_id'=> $group->connect_id]);
        return $this->render('group_process', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'keywords' => $keywords,
            'model' => $group,
        ]);
    }

    public function actionUsedGroup()
    {
        $status = KeywordGroups::STATUS_ISPOLZOVANNIE;
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('used_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }

    public function actionGroup()
    {

     foreach ($_POST['keylist'] as $key){
           $model = Keywords::findOne(['id' => $key]);
           $model->group_id = $_POST['id'][0];
           $model->color = 0;
           $model->save();

           $old_group = KeywordGroups::findOne(['id' => $_POST['model']]);
           $old_group->frequency -= $model->frequency;
           $old_group->save();

           $new_group = KeywordGroups::findOne(['connect_id' => $_POST['id'][0]]);
           $new_group->project = $old_group->project;
           $new_group->frequency += $model->frequency;
           $new_group->save();
       }
        Yii::$app->getSession()->setFlash('success', 'Ключевики перешли  в другую группу');
        return true;
    }

    public function actionOffer()
    {
        $use = 1;
        $keywds = Keywords::find()->where(['group_id' => $_POST['id']])->all();
        foreach ($keywds as $keys){
            if($keys->color != 0 && $keys->color > $use){
                $use = $keys->color;
            }
        }
        $use++;
        if($use == 10){
            $use = 1;
        }
        foreach ($_POST['keylist'] as $key){
            $model = Keywords::findOne(['id' => $key]);
            $group = KeywordGroupsSearch::findOne(['connect_id' => $model->group_id]);
            $model->color = $use;
            $model->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Успешно');
        return true;
    }

    public function actionNewGroup()
    {
        $newGroup = new KeywordGroups();
        $count = 0;
        if ($_POST) {
            $group_id = (new \yii\db\Query())
                ->select(['max(connect_id)'])
                ->from('keyword_groups')
                ->scalar();

            $color_count = (new \yii\db\Query())
                ->select(['color'])
                ->from('keyword_groups')
                ->orderBy(['id' => SORT_DESC])
                ->limit(1)
                ->scalar();

            $group_id += 11;
            $color_count++;
            if($color_count <= 10){
                $newGroup->color = $color_count;
            }else{
                $newGroup->color = 1;
            }

            $newGroup->name = $_POST['name'];
            $newGroup->status = KeywordGroups::STATUS_V_RABOTE;

            $newGroup->semantik = Yii::$app->user->getId();
            $newGroup->connect_id = $group_id;
            $newGroup->save();

            foreach ($_POST['keylist'] as $key){
                $count++;
                $model = Keywords::findOne(['id' => $key]);
                $model->group_id = $group_id;


                $old_group = KeywordGroups::findOne(['id' => $_POST['model']]);
                $old_group->frequency -= $model->frequency;
                $old_group->save();

                if($count == 1){

                    if((int)$model->frequency > 300){
                        $newGroup->import_key = 2;
                    }

                    if((int)$model->frequency <= 300){
                        $newGroup->import_key = 1;
                    }
                }

                $newGroup->project = $old_group->project;
                $newGroup->frequency += $model->frequency;
                $newGroup->save();
            }
            Yii::$app->getSession()->setFlash('success', 'Создано новая группа');
            return true;
          }

        Yii::$app->getSession()->setFlash('error', 'Ошибка при создание новой группы');
        return false;
    }

    public function actionDeleteKey()
    {
        foreach ($_POST['keylist'] as $key) {

            $model = Keywords::findOne(['id' => $key]);
            $gid = $model->group_id;
            $model->status = Keywords::STATUS_DELETED;
            $model->group_id = null;
            $model->save(false);
            $group = KeywordGroups::findOne(['connect_id' => $gid]);
            $group->frequency -= $model->frequency;
            $group->save(false);

        }
        Yii::$app->getSession()->setFlash('success','Ключевики удалены!' );
        return true;
    }

    public function actionFinalGroup()
    {
        $status = KeywordGroups::STATUS_GOTOVIE_GRUPPI;
        $searchModel = new KeywordGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status' => $status]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('final_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => KeywordGroups::getStatusName($status),
        ]);
    }
}
